# Copyright 2013-2020 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)


from spack import *
import os

class Softposit(MakefilePackage):
    """SoftPosit (C code with C++ support).

       This is a fast posit C implementation of by S.H. Leong (Cerlane).
       It includes a C++ wrapper to override operators (slower) for easier use."""

    homepage = "https://gitlab.com/cerlane/SoftPosit"
    url      = "https://gitlab.com/cerlane/SoftPosit/-/archive/0.4.1/SoftPosit-0.4.1.tar.gz"
    git      = "https://gitlab.com/cerlane/SoftPosit.git"

    maintainers = ['jlow2016']

    version('develop', branch='master')
    version('0.4.1',  sha256='13f7360c5b91ad3704f66537a754ba3748a764e1291eaa33940866ca37c7dbf5')

    variant('opt', default=True, description='Build with -O2 optimisation.')
    variant('shared', default=False, description='Build shared library.')
    variant('quad', default=False, description='Build quad precision version of library.')

    def edit(self, spec, prefix):
        makefile = FileFilter(join_path(self.build_directory, 'build/Linux-x86_64-GCC/Makefile'))

        # Use Spack variables
        makefile.filter(r'^SOURCE_DIR \?= .*', 'SOURCE_DIR = %s' % join_path(self.build_directory, 'source'))
        makefile.filter(r'^COMPILER \?= .*', 'COMPILER = %s' % spack_cc)
        # Use new compiler cc_pic_flag attribute on Spack versions > 0.14.2
        try:
            makefile.filter('-fPIC', self.compiler.cc_pic_flag)
        except AttributeError:
            makefile.filter('-fPIC', self.compiler.pic_flag)

        makefile.filter('OPTIMISATION  =.*', 'OPTIMISATION  = {0}'.format('-O2' if '+opt' in spec else ''))

        if '+quad' in spec and '+shared' in spec:
            makefile.filter(r'^quad: SOFTPOSIT_OPTS\+=.*', 'quad: SOFTPOSIT_OPTS+= -DSOFTPOSIT_QUAD -lquadmath {0}'.format(self.compiler.pic_flag))

    def build(self, spec, prefix):
        with working_dir(join_path(self.build_directory, 'build/Linux-x86_64-GCC')):
            if '+quad' in spec:
                make('quad')

            if '+shared' in spec and '+quad' in spec:
                make('softposit.so')
            elif '+shared' in spec:
                make('julia')
                make('all')
            elif '~quad' in spec:
                make('all')

    def install(self, spec, prefix):
        # Manual installation
        with working_dir(prefix.lib, create=True):
            install(join_path(self.build_directory, 'build/Linux-x86_64-GCC/softposit.a'), 'libsoftposit.a')

            if '+shared' in spec:
                install(join_path(self.build_directory, 'build/Linux-x86_64-GCC/softposit.so'), 'libsoftposit.so')

            # Create symlink to retain compatibility
            os.symlink('libsoftposit.a', 'softposit.a')
            if '+shared' in spec:
                os.symlink('libsoftposit.so', 'softposit.so')

        install_tree(join_path(self.build_directory, 'source/include'), prefix.include)
