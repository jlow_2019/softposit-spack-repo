# SoftPosit Spack repo

SoftPosit repo for [Spack](https://github.com/spack/spack) users.

Packages available:
* [softposit](https://gitlab.com/cerlane/SoftPosit)
* [softposit-math](https://gitlab.com/cerlane/softposit-math)
* [py-softposit](https://gitlab.com/cerlane/SoftPosit-Python)
* [py-softfloat](https://gitlab.com/cerlane/SoftFloat-Python)

## Quickstart

(1) Setup Spack and add the SoftPosit repo:

```
git clone https://github.com/spack/spack.git
git clone https://gitlab.com/jlow_2019/softposit-spack-repo.git
cd spack/bin
./spack repo add ../../softposit-spack-repo
```

(2) Install SoftPosit:

```
./spack install softposit
```
