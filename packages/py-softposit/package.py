# Copyright 2013-2020 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *


class PySoftposit(PythonPackage):
    """This is the python wrapper (using SWIG) of SoftPosit."""

    homepage = "https://gitlab.com/cerlane/SoftPosit-Python"
    url      = "https://gitlab.com/cerlane/SoftPosit-Python/-/archive/v0.1/SoftPosit-Python-v0.1.tar.gz"
    git      = "https://gitlab.com/cerlane/SoftPosit-Python.git"

    maintainers = ['jlow2016']

    version('develop', branch='master')
    version('0.1', preferred=True, sha256='cd6f47940b47720b0a5f5c6a7927c2f07f8fb4d6e54ed5bcbcb26c366d4bf2cc')

    import_modules = ['softposit']

    depends_on('python', type=('build', 'run'))
    depends_on('py-setuptools', type='build')
    depends_on('py-requests', when='@0.1', type='build')

    build_directory = 'softposit'
