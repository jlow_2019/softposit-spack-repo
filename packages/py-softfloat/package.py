# Copyright 2013-2020 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *


class PySoftfloat(PythonPackage):
    """This is the python wrapper (using SWIG) of Berkeley SoftFloat (modified version of Release 3d)."""

    homepage = "https://gitlab.com/cerlane/SoftFloat-Python"
    git      = "https://gitlab.com/cerlane/SoftFloat-Python.git"

    maintainers = ['jlow2016']

    version('develop', branch='master')

    import_modules = ['softfloat']

    depends_on('python', type=('build', 'run'))
    depends_on('py-setuptools', type='build')

    build_directory = 'softfloat'
