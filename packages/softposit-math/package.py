# Copyright 2013-2020 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)


from spack import *
import os


class SoftpositMath(MakefilePackage):
    """Prototype SoftPosit libM library.

       This will in time (hopefully) contains all the libMath functions for posits."""

    homepage = "https://gitlab.com/cerlane/softposit-math"
    git      = "https://gitlab.com/cerlane/softposit-math.git"

    maintainers = ['jlow2016']

    version('develop', branch='master')

    variant('opt', default=True, description='Build with -O3 optimisation.')
    variant('shared', default=False, description='Build shared library.')
    variant('benchmarks', default=False, description='Build micro-benchmarks. Requires OpenMP support.')

    depends_on('softposit', type=('build', 'link'))

    def edit(self, spec, prefix):
        makefile = FileFilter(join_path(self.build_directory, 'build/Makefile'))

        makefile.filter(r'^C_INCLUDES = .*', 'C_INCLUDES = -I{0} -I{1}'.format(join_path(self.build_directory, 'include'), spec['softposit'].prefix.include))
        makefile.filter(r'^LIBS = .*', 'LIBS = softposit_math.a {0} -lm'.format(join_path(spec['softposit'].prefix.lib, 'softposit.a')))

        # Use Spack variables
        makefile.filter(r'^SOURCE_DIR \?= .*', 'SOURCE_DIR = %s' % join_path(self.build_directory, 'source'))
        makefile.filter(r'^CC = .*', 'CC = %s' % spack_cc)
        makefile.filter('-fopenmp', self.compiler.openmp_flag)
        # Use new compiler cc_pic_flag attribute on Spack versions > 0.14.2
        try:
            makefile.filter('-fPIC', self.compiler.cc_pic_flag)
        except AttributeError:
            makefile.filter('-fPIC', self.compiler.pic_flag)

        # Architecture optimization is done via. Spack
        makefile.filter(r'^COPTS = .*', 'COPTs = {0}'.format('-O3' if '+opt' in spec else ''))

        # Insert shared library compilation code
        makefile.filter(r'ar crs softposit_math.a \*.o\n', 'ar crs softposit_math.a *.o{0}'.format('; $(CC) -shared -o libsoftposit-math.so *.o \n' if '+shared' in spec else '\n'))

    def build(self, spec, prefix):
        with working_dir(join_path(self.build_directory, 'build')):
            make('all')

            if '+benchmarks' in spec:
                make('benchmarks')

    def check(self):
        with working_dir(join_path(self.build_directory, 'build')):
            make('check', parallel=False)

    def installcheck(self):
        pass

    def install(self, spec, prefix):
        # Manual installation
        with working_dir(prefix.lib, create=True):
            install(join_path(self.build_directory, 'build/softposit_math.a'), 'libsoftposit-math.a')
            if '+shared' in spec:
                install(join_path(self.build_directory, 'build/libsoftposit-math.so'), 'libsoftposit-math.so')

            # Create symlink to retain backward compatibility
            os.symlink('libsoftposit-math.a', 'softposit_math.a')
            if '+shared' in spec:
                os.symlink('libsoftposit-math.so', 'softposit_math.so')

        install_tree(join_path(self.build_directory, 'include'), prefix.include)

        if '+benchmarks' in spec:
            with working_dir(prefix.bin, create=True):
                for filename in os.listdir(join_path(self.build_directory, 'build')):
                    if filename.startswith("benchmark"):
                        install(join_path(self.build_directory, 'build/%s' % filename), filename)
